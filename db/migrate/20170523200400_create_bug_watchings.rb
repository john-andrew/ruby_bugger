class CreateBugWatchings < ActiveRecord::Migration[5.0]
  def change
    create_table :bug_watchings do |t|
      t.integer :bug_id
      t.integer :user_id

      t.timestamps
    end
    add_index :bug_watchings, :bug_id
    add_index :bug_watchings, :user_id
    add_index :bug_watchings, [:bug_id, :user_id], unique: true
  end
end
