class AddResolverRefToBugs2 < ActiveRecord::Migration[5.0]
  def change
    add_reference :bugs, :resolver, foreign_key: {to_table: :users}
  end
end
