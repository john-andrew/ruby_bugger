class ChangeResolvedInBugsToDefaultFalse < ActiveRecord::Migration[5.0]
  def change
    change_column_default :bugs, :resolved, false
  end
end
