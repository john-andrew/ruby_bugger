class CreateBugs < ActiveRecord::Migration[5.0]
  def change
    create_table :bugs do |t|
      t.string :name
      t.text :error_msg
      t.text :description
      t.text :causes
      t.integer :user_id
      t.boolean :resolved
      t.integer :resolved_user_id

      t.timestamps
    end
  end
end
