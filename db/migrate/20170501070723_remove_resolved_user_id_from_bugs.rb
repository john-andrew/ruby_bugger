class RemoveResolvedUserIdFromBugs < ActiveRecord::Migration[5.0]
  def change
    remove_column :bugs, :resolved_user_id, :integer
  end
end
