class User < ApplicationRecord
  include ActiveModel::Validations

  # Associations
  has_many :bugs, dependent: :destroy
  has_many :watched_bugs, through: :bug_watchings, source: :bug_id
  has_many :bug_watchings, class_name: "BugWatching",
                           foreign_key: "user_id",
                           dependent: :destroy

  # Ordering
  default_scope -> { order(username: :asc) }

  # Constants
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  # Callbacks
  before_save { self.email = email.downcase }
  
  # Object Attribute Validation
  validates_with UserEmailConfirmationValidator

  attr_accessor :email_confirmation
  validates :email_confirmation, presence: true
   
  # Model Validation
  validates :username, presence: true,
                       length: {maximum: 50},
                       uniqueness: {case_sensitive: false}
  validates :first_name, presence: true, length: {maximum: 50}
  validates :last_name, presence: true, length: {maximum: 50}
  validates :email, presence: true,
                    length: {maximum: 255},
                    format: {with: VALID_EMAIL_REGEX},
                    uniqueness: {case_sensitive: false}
  has_secure_password                  
  validates :password, presence: true,
                       length: {minimum: 8}
  validates :password_confirmation, presence: true
  
  
end