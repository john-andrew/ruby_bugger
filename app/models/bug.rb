class Bug < ApplicationRecord
  # Associations
  belongs_to :user
  belongs_to :resolver, class_name: 'User', optional: true
  has_many :watchers, through: :bug_watchings, source: :user_id
  has_many :bug_watchings, class_name: "BugWatching",
                           foreign_key: "bug_id",
                           dependent: :destroy

  # Ordering
  default_scope -> { order(created_at: :desc) }

  # Model Validation
  validates :name, presence: true, length: {maximum: 50}
  validates :description, presence: true
  validates :causes, presence: true
  validates :user_id, presence: true
end