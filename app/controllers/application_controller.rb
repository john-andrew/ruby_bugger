class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  private
    # Checks if a user is logged-in
    def logged_in_user
      unless logged_in?
        store_accessed_url
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Returns false if current_user does not match given user
    def correct_user?(user)
      return user == current_user 
    end

    def user_object_not_found
      render plain: "User object not found"
    end

    def permission_error
      render plain: "You don't have permission to do that"
    end

    # Returns true if user is an admin, else returns false
    def admin_user?
      if current_user
        return current_user.admin?
      end
    end

    # Returns true if user is developer, else returns false
    def developer_user?
      if current_user
        return current_user.developer?
      end
    end

    # Returns permission_error if current user is not an admin
    def verify_admin_user
      unless admin_user?
        permission_error
      end
    end

      # Retuns permission_error if current user is not a developer
    def verify_developer_user
      unless developer_user?
        permission_error
      end
    end

end
