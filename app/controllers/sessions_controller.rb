class SessionsController < ApplicationController
  def new
    if logged_in?
      redirect_to url_for Bug
    end
  end

  def create
    username = params[:session][:username]
    password = params[:session][:password]
    user = User.find_by(username: username)

    if user && user.authenticate(password)
      log_in user
      redirect_back_or url_for Bug
    else
      flash[:danger] = 'Invalid username or password'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
