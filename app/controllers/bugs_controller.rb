class BugsController < ApplicationController
  before_action :find_bug, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user
  
  def index
    if admin_user? || developer_user?
      @bugs = Bug.all
    else
      @bugs = current_user.bugs.all
    end
  end

  def show
  end

  def new
    @bug = Bug.new
  end

  def create
    @bug = current_user.bugs.build(bug_params)
    if @bug.save
      redirect_to url_for Bug
    else
      render 'new'
    end
  end

  def edit
  end
  
  def update
    set_resolver
    if @bug.update(bug_params)
      redirect_to bug_path(@bug)
    else
      render 'edit'
    end
  end
  
  def destroy
    @bug.destroy
    redirect_to url_for Bug
  end

  private
    def bug_params
      params.require(:bug).permit(:name, :error_msg, :description, :causes, :resolved, :resolver_id)
    end

    def find_bug
      if admin_user? || developer_user?
        @bug = Bug.find_by(id: params[:id])
      else
        @bug = current_user.bugs.find_by(id: params[:id])
      end
      
      if @bug.nil?
        user_object_not_found
      end
    end

    def set_resolver
      if params[:bug][:resolved] == '1' && @bug.resolved == false
        verify_developer_user
        params[:bug][:resolver_id] = current_user.id
        puts current_user
      elsif params[:bug][:resolved] == '0' || @bug.resolved == false
        params[:bug][:resolver_id] = nil
      end
    end
end
