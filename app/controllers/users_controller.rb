class UsersController < ApplicationController
  before_action :find_user, only: [:show, :edit, :update, :destroy]
  before_action :verify_admin_user, only: [:index, :destroy]

  def index
    @users = User.all
  end

  def show
    if admin_user?
      puts "redirect to user"
      redirect_to url_for users_path
    else
      puts "redirect to bugs"
      redirect_to url_for bugs_path
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      if admin_user?
        redirect_to url_for users_path
      else
        flash[:success] = 'You are now registered, please log-in!'
        redirect_to url_for login_path
      end
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:edit_privileges]
      # Case: Privileges
      params[:user][:username] = @user.username
      params[:user][:password] = @user.password_digest
      params[:user][:password_confirmation] = @user.password_digest
      params[:user][:email_confirmation] = @user.email    
    end

    # Update
    if @user.update(user_params)
      redirect_to user_path(@user)
    else
      render 'edit'
    end
  end
  
  def destroy
    @user.destroy
    redirect_to url_for users_path
  end

  private
    def user_params
      params.require(:user).permit(:username, :first_name,:last_name,
                                   :email, :email_confirmation, :password,
                                   :password_confirmation, :admin,
                                   :developer)
    end

    def find_user
      @user = User.find_by(id: params[:id])
      if @user.nil?
        user_object_not_found
      end

      unless admin_user?
        if !correct_user?(@user)
          permission_error
        end
      end
    end
end
