class UserEmailConfirmationValidator < ActiveModel::Validator
  def validate(record)
    if record.email != record.email_confirmation
      record.errors.add :email_confirmation, 'is not the same email'
    end
  end
end