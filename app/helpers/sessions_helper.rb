module SessionsHelper

  # Logs in a given user
  def log_in(user)
    session[:user_id] = user.id
  end

  # Logs out current user
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  # Returns current logged-in user or nil
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  # Returns true if the user is logged in, else returns false
  def logged_in?
    !current_user.nil?
  end

  # Redirects to either a stored location or a default location
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed in the session
  def store_accessed_url
    session[:forwarding_url] = request.original_url if request.get?
  end

end
